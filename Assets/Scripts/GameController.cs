﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GameController : MonoBehaviour
{
    [SerializeField] int level;
    [SerializeField] bool isDialogue;
    SaveManager save;
    DialoguePanel dialogue;

    public int Level { get => level; set => level = value; }
    public bool IsDialogue { get => isDialogue; set => isDialogue = value; }

    void Awake()
    {
        save = FindObjectOfType<SaveManager>();
        dialogue = FindObjectOfType<DialoguePanel>();
    }

    void Start()
    {
        //загружаем сохранение
        //save.Load();

        if (IsDialogue)
        {
            dialogue.ShowDialog(level);
        }
    }
}
