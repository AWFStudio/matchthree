﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO; //чтобы сохранять в файл

public class SaveManager : MonoBehaviour
{
    //уровень
    int level = 0;

    //показывать ли диалог
    bool isDialogue = true;

    //показывать ли обучение
    bool isTraining = true;

    //строка данных
    string data;

    //ссылки на другие скрипты
    GameController game;
    LevelController levelController;

    public void Awake()
    {
        game = FindObjectOfType<GameController>();
        levelController = FindObjectOfType<LevelController>();
    }

    //собираем данные
    public void CollectData()
    {
        if (game)
        {
            level = game.Level;
            isDialogue = game.IsDialogue;
        }
        else if (levelController)
        {
            isTraining = levelController.IsTraining;
        }
    }

    //раздаем данные 
    public void SetData()
    {
        if (game)
        {
            game.Level = level;
            game.IsDialogue = isDialogue;
        }
        else if (levelController)
        {
            levelController.IsTraining = isTraining;
        }
    }

    //сохраняем
    public void Save()
    {
        //собираем данные
        CollectData();
        //преобразовываем этот документ в Json
        data = JsonUtility.ToJson(this);
        //записываем сохранение в файл
        File.WriteAllText("D:/UnityProjects/Saves/save.txt", data);
    }

    //загружаем
    public void Load()
    {
        //если есть файл сохранения, выгружаем его
        if (File.Exists("D:/UnityProjects/Saves/save.txt"))
        {
            data = File.ReadAllText("D:/UnityProjects/Saves/save.txt");
            JsonUtility.FromJsonOverwrite(data, this);
            SetData();
        }
    }
}
