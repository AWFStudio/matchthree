﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChainFinder : MonoBehaviour
{
    LevelController controller;
    GameObject levelContainer;

    //ищем цепочки
    public List<GameObject> CheckForAssembledChains()
    {
        if (!controller)
        {
            controller = FindObjectOfType<LevelController>();
        }
        if (!levelContainer)
        {
            levelContainer = controller.LevelContainer;
        }

        List<GameObject> tilesToDestroy = new List<GameObject>();

        for (int i = 0; i < levelContainer.transform.childCount; i++)
        {
            if (levelContainer.transform.GetChild(i).GetComponent<Cell>())
            {
                if (FindAllMatch(levelContainer.transform.GetChild(i).gameObject))
                {
                    tilesToDestroy.Add(levelContainer.transform.GetChild(i).gameObject);
                }
            }
        }
        return tilesToDestroy;
    }

    private bool FindAllMatch(GameObject cell)
    {
        //ищем все цепочки одинаковых тайлов (от 3 и более)
        bool verticalChainCount = CatchChain(cell, new Vector2[2] { Vector2.up, Vector2.down });
        bool horizontalChainCount = CatchChain(cell, new Vector2[2] { Vector2.left, Vector2.right });
        //если цепочки найдены
        if (verticalChainCount || horizontalChainCount)
        {
            //сообщаем скрипту перемещения тайла, что сдвигать тайл обратно не надо
            return true;
        }
        return false;
    }

    private bool CatchChain(GameObject cell, Vector2[] dirArray)
    {
        List<GameObject> chain = new List<GameObject>();
        for (int i = 0; i < dirArray.Length; i++)
        {
            chain.AddRange(FindChainElement(cell, dirArray[i]));
        }
        //если в полученном списке содержится минимум два элемента, значит у нас есть три тайла, которые собраны в ряд...
        //три, поскольку тайл ячейки, из которой стреляли, тоже учитывается
        if (chain.Count >= 2)
        {
            return true;
        }
        return false;
    }

    private List<GameObject> FindChainElement(GameObject cell, Vector2 dir)
    {
        List<GameObject> tempArray = new List<GameObject>();
        RaycastHit2D hit = Physics2D.Raycast(cell.transform.position, dir);
        //если на пути луча попадается ячейка, и если ее тайл соответствует тайлу ячейки, из которой мы стреляем...
        while (hit.collider != null
            && hit.collider.gameObject.GetComponent<Cell>()
            && hit.collider.gameObject.GetComponent<Cell>().GetTile().GetComponent<Image>().sprite
            && hit.collider.gameObject.GetComponent<Cell>().GetTile().GetComponent<Image>().sprite == cell.GetComponent<Cell>().GetTile().GetComponent<Image>().sprite)
        {
            //то мы заносим эту ячейку в список
            tempArray.Add(hit.collider.gameObject);
            //и стреляем из него дальше в том же направлении
            hit = Physics2D.Raycast(hit.collider.gameObject.transform.position, dir);
        }
        return tempArray;
    }
}
