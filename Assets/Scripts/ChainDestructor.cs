﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChainDestructor : MonoBehaviour
{
    LevelData data;
    EmptySellsFiller filler;
    [SerializeField] GameObject bonusPref;
    GameObject cell;
    List<GameObject> chain;
    bool isHorizontal;

    private void Awake()
    {
        data = FindObjectOfType<LevelData>();
        filler = FindObjectOfType<EmptySellsFiller>();
    }

    public void ChainsDestruction(List<GameObject> chain, GameObject cell, bool isHorizontal)
    {
        this.chain = chain;
        this.cell = cell;
        this.isHorizontal = isHorizontal;

        StartCoroutine(Destroyer());
    }

    bool CheckLevelGoals(List<GameObject> chain)
    {
        if (data.CollectionSprite_1 == chain[0].GetComponent<Cell>().GetTile().GetComponent<Image>().sprite)
        {
            for (int i = 0; i < chain.Count; i++)
            {
                chain[i].GetComponent<Cell>().GetTile().GetComponent<Tile>().CollectThisTile(0);
            }
            return true;
        }
        else if (data.CollectionSprite_2 == chain[0].GetComponent<Cell>().GetTile().GetComponent<Image>().sprite)
        {
            for (int i = 0; i < chain.Count; i++)
            {
                chain[i].GetComponent<Cell>().GetTile().GetComponent<Tile>().CollectThisTile(1);
            }
            return true;
        }
        else if (data.CollectionSprite_3 == chain[0].GetComponent<Cell>().GetTile().GetComponent<Image>().sprite)
        {
            for (int i = 0; i < chain.Count; i++)
            {
                chain[i].GetComponent<Cell>().GetTile().GetComponent<Tile>().CollectThisTile(2);
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    IEnumerator Destroyer()
    {
        bonus = BonusCheck();

        if (!CheckLevelGoals(chain))
        {
            for (int i = 0; i < chain.Count; i++)
            {
                if (bonus != null)
                {
                    chain[i].GetComponent<Cell>().GetTile().GetComponent<Tile>().MoveToBonusPos(cell);
                }
                Destroy(chain[i].GetComponent<Cell>().GetTile());
            }
        }
        yield return null;
        filler.FillCells();
    }

    public GameObject BonusCheck()
    {
        GameObject bonus;

        if (chain.Count == 4)
        {
            bonus = Instantiate(bonusPref);
            Debug.Log("bonus = " + bonus);
            //даём ракеты по горизонтали/вертикали
            if (isHorizontal)
            {
                bonus.GetComponent<Bonus>().type = Bonus.BonusType.horizontal;
            }
            else
            {
                bonus.GetComponent<Bonus>().type = Bonus.BonusType.vertical;
            }
        }
        else if (chain.Count == 5)
        {
            //даём динамит (действие?)
            bonus = Instantiate(bonusPref);
            bonus.GetComponent<Bonus>().type = Bonus.BonusType.bomb;
        }
        //if (isBonus)
        //{
        //    bonus.transform.SetParent(cell.transform, false);
        //    bonus.GetComponent<Animation>().Play("Bonus Appearance");
        //}

        return bonus;
    }



}
