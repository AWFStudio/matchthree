﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Cell : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    int hPos;
    int wPos;

    [SerializeField] bool isSelected = false;

    LevelController controller;
    GameObject frameImg;
    Vector3 startPos;

    public GameObject FrameImg { get => frameImg; set => frameImg = value; }
    public int HPos { get => hPos; set => hPos = value; }
    public int WPos { get => wPos; set => wPos = value; }
    public bool IsSelected { get => isSelected; set => isSelected = value; }

    void Awake()
    {
        controller = FindObjectOfType<LevelController>();
        frameImg = transform.GetChild(0).gameObject;
    }

    public GameObject GetTile()
    {
        if (transform.childCount > 1 && transform.GetChild(1).GetComponent<Tile>())
        {
            return transform.GetChild(1).gameObject;
        }
        else
        {
            return null;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //если сейчас можно делать ход
        if (!controller.IsTurn)
        {
            //если эта ячейка еще не выделена
            if (!IsSelected)
            {
                //если нет другой выделенной ячейки
                if (controller.SelectedCell == null)
                {
                    IsSelected = true;
                    controller.SelectedCell = gameObject;
                    ShowHideFrame();
                }
                else
                {
                    //если эта ячейка соседствует с ранее выделенной ячейкой, то их можно менять местами
                    List<Vector2> directions = new List<Vector2> { Vector2.right, Vector2.left, Vector2.down, Vector2.up };

                    for (int i = 0; i < directions.Count; i++)
                    {
                        RaycastHit2D hit = Physics2D.Raycast(transform.position, directions[i]);
                        if (hit.collider != null && hit.collider.gameObject.GetComponent<Cell>().gameObject == controller.SelectedCell)
                        {
                            //обращаемся к дочернему компоненту ячейки (тайлу) и запускаем в нем карутину смены местоположения и родителя

                            GetTile().GetComponent<Tile>().MoveTo(controller.SelectedCell.gameObject, true);
                            controller.SelectedCell.GetComponent<Cell>().GetTile().GetComponent<Tile>().MoveTo(gameObject, false);
                            controller.SelectedCell.GetComponent<Cell>().IsSelected = false;
                            controller.SelectedCell.GetComponent<Cell>().ShowHideFrame();
                            controller.SelectedCell = null;
                            break;
                        }
                    }
                    if (controller.SelectedCell != null)
                    {
                        //иначе снимаем выделение с ранее выделенной ячейки и выделяем эту
                        controller.SelectedCell.GetComponent<Cell>().IsSelected = false;
                        controller.SelectedCell.GetComponent<Cell>().ShowHideFrame();
                        IsSelected = true;
                        controller.SelectedCell = gameObject;
                        ShowHideFrame();
                    }
                }
            }
            //при повторном клике на выделенной ячейке, снимаем с нее выделение
            else
            {
                IsSelected = false;
                controller.SelectedCell = null;
                ShowHideFrame();
            }
        }
    }

    public void ShowHideFrame()
    {
        if (IsSelected)
        {
            FrameImg.SetActive(true);
        }
        else
        {
            FrameImg.SetActive(false);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        startPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (!controller.IsTurn)
        {
            Vector3 newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            float[] directions = new float[4];

            if (startPos == newPos)
            {
                return;
            }

            if (startPos.x > newPos.x)
            {
                directions[1] = startPos.x - newPos.x;
            }
            else if (startPos.x < newPos.x)
            {
                directions[0] = newPos.x - startPos.x;
            }
            if (startPos.y > newPos.y)
            {
                directions[2] = startPos.y - newPos.y;
            }
            else if (startPos.y < newPos.y)
            {
                directions[3] = newPos.y - startPos.y;
            }

            float maxPath = 0;
            int dir = 0;

            for (int i = 0; i < directions.Length; i++)
            {
                if (directions[i] > maxPath)
                {
                    maxPath = directions[i];
                    dir = i;
                }
            }

            if (dir == 0)
            {
                //меняем тайл местами с тайлом справа
                RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right);
                if (hit.collider != null && hit.collider.gameObject.GetComponent<Cell>())
                {
                    GetTile().GetComponent<Tile>().MoveTo(hit.collider.gameObject, true);
                    hit.collider.gameObject.GetComponent<Cell>().GetTile().GetComponent<Tile>().MoveTo(gameObject, false);
                }
            }
            else if (dir == 1)
            {
                //меняем тайл местами с тайлом слева
                RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.left);
                if (hit.collider != null && hit.collider.gameObject.GetComponent<Cell>())
                {
                    GetTile().GetComponent<Tile>().MoveTo(hit.collider.gameObject, true);
                    hit.collider.gameObject.GetComponent<Cell>().GetTile().GetComponent<Tile>().MoveTo(gameObject, false);
                }
            }
            else if (dir == 2)
            {
                //меняем тайл местами с тайлом снизу
                RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down);
                if (hit.collider != null && hit.collider.gameObject.GetComponent<Cell>())
                {
                    GetTile().GetComponent<Tile>().MoveTo(hit.collider.gameObject, true);
                    hit.collider.gameObject.GetComponent<Cell>().GetTile().GetComponent<Tile>().MoveTo(gameObject, false);
                }
            }
            else if (dir == 3)
            {
                //меняем тайл местами с тайлом сверху
                RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up);
                if (hit.collider != null && hit.collider.gameObject.GetComponent<Cell>())
                {
                    GetTile().GetComponent<Tile>().MoveTo(hit.collider.gameObject, true);
                    hit.collider.gameObject.GetComponent<Cell>().GetTile().GetComponent<Tile>().MoveTo(gameObject, false);
                }
            }

            if (controller.SelectedCell != null)
            {
                controller.SelectedCell.GetComponent<Cell>().IsSelected = false;
                controller.SelectedCell.GetComponent<Cell>().ShowHideFrame();
                controller.SelectedCell = null;
            }
        }




    }
}