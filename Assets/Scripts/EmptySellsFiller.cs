﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmptySellsFiller : MonoBehaviour
{
    LevelController controller;
    GameObject levelContainer;
    LevelData data;
    ChainFinder finder;
    ChainDestructor destructor;

    public void FillCells()
    {
        SetValues();
        StartCoroutine(Enumerator());
    }
    void SetValues()
    {
        if (!controller)
        {
            controller = FindObjectOfType<LevelController>();
        }
        if (!levelContainer)
        {
            levelContainer = controller.LevelContainer;
        }
        if (!data)
        {
            data = FindObjectOfType<LevelData>();
        }
        if (!finder)
        {
            finder = FindObjectOfType<ChainFinder>();
        }
        if (!destructor)
        {
            destructor = FindObjectOfType<ChainDestructor>();
        }
    }

    List<Cell> FindEmptySells()
    {
        List<Cell> emptyCells = new List<Cell>();

        for (int i = 0; i < levelContainer.transform.childCount; i++)
        {
            if (levelContainer.transform.GetChild(i).GetComponent<Cell>()
                && levelContainer.transform.GetChild(i).GetComponent<Cell>().GetTile() == null)
            {
                emptyCells.Add(levelContainer.transform.GetChild(i).GetComponent<Cell>());
            }
        }
        return emptyCells;
    }

    void CheckPositions(List<Cell> emptyCells)
    {
        for (int i = 0; i < emptyCells.Count; i++)
        {
            //стреляем вверх из пустой ячейки
            RaycastHit2D hit = Physics2D.Raycast(emptyCells[i].transform.position, Vector2.up);
            //если над нашей ячейкой есть еще одна
            if (hit.collider != null && hit.collider.gameObject.GetComponent<Cell>())
            {
                //и в ней есть тайл
                if (hit.collider.gameObject.GetComponent<Cell>().GetTile() != null)
                {
                    //находим тайл, который лежит в ней и вызываем на нем метод смещения
                    hit.collider.gameObject.GetComponent<Cell>().GetTile().GetComponent<Tile>().MoveTo(emptyCells[i].gameObject, false);
                }
            }
            else
            {
                //копируем список возможных спрайтов из данных уровня
                List<Sprite> tempList = new List<Sprite>();
                tempList.AddRange(data.AllSprites);
                //создаем список направлений
                List<Vector2> directions = new List<Vector2> { Vector2.right, Vector2.left, Vector2.down, Vector2.up };
                //стреляем во всех направлениях
                for (int f = 0; f < directions.Count; f++)
                {
                    RaycastHit2D hit1 = Physics2D.Raycast(emptyCells[i].transform.position, directions[f]);
                    //если попали в ячейку, и она не пустая
                    if (hit1.collider != null && hit1.collider.gameObject.GetComponent<Cell>()
                        && hit1.collider.gameObject.GetComponent<Cell>().GetTile() != null)
                    {
                        //стреляем из нее дальше
                        RaycastHit2D hit2 = Physics2D.Raycast(hit1.transform.position, directions[f]);
                        //и если снова попали в ячейку, и она не пустая
                        if (hit2.collider != null && hit2.collider.gameObject.GetComponent<Cell>()
                            && hit2.collider.gameObject.GetComponent<Cell>().GetTile() != null)
                        {
                            //и если у них одинаковые спрайты
                            if (hit1.collider.GetComponent<Cell>().GetTile().GetComponent<Image>().sprite
                                == hit2.collider.GetComponent<Cell>().GetTile().GetComponent<Image>().sprite)
                            {
                                //тогда убираем этот спрайт из списка доступных
                                tempList.Remove(hit1.collider.GetComponent<Cell>().GetTile().GetComponent<Image>().sprite);
                            }
                        }
                    }
                }

                //инстанцируем тайл
                GameObject tileImg = Instantiate(Resources.Load<GameObject>("Prefabs/Tile"));
                tileImg.transform.SetParent(emptyCells[i].gameObject.transform, false);
                //помещаем в него рандомный спрайт из оставшихся
                tileImg.GetComponent<Image>().sprite = tempList[Random.Range(0, tempList.Count)];
            }
        }
    }

    IEnumerator Enumerator()
    {
        List<Cell> emptyCells;
        do
        {
            emptyCells = new List<Cell>();
            //ищем пустые ячейки и заносим их в массив
            emptyCells.AddRange(FindEmptySells());
            if (emptyCells.Count > 0) //если массив не пуст...
            {
                CheckPositions(emptyCells); //передаем его в метод, заполняющий пустые ячейки
            }
            yield return null;
        }
        while (emptyCells.Count > 0); //повторяем, пока есть пустые ячейки

        List<GameObject> tilesToDestroy = new List<GameObject>();
        tilesToDestroy.AddRange(finder.CheckForAssembledChains());

        //если новых цепочек нет
        if (tilesToDestroy.Count == 0)
        {
            //разрешаем пользователю снова перемещать фишки
            controller.IsTurn = false;
        }
        else //иначе
        {
            //вызываем метод уничтожения сложившихся цепочек
            destructor.ChainsDestruction(tilesToDestroy, null, false);
        }
    }
}

