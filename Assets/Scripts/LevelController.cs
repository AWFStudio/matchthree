﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
    StartRoundPanel round;
    InfoPanel info;
    TooltipPanel tooltip;
    GameObject selectedCell = null;
    GameObject levelContainer;
    LevelData data;
    [SerializeField] bool isTurn;

    //временно сериализованы:
    [SerializeField] int level; //текущий уровень
    [SerializeField] bool isTraining; //показывать ли обучение

    [SerializeField] GameObject boardPanel; //панелька игрового поля (внутрь нее помещаем префаб уровня)
    [SerializeField] GameObject startRoundPanel; //панелька начала раунда

    public int Level { get => level; set => level = value; }
    public bool IsTraining { get => isTraining; set => isTraining = value; }
    public GameObject SelectedCell { get => selectedCell; set => selectedCell = value; }
    public GameObject LevelContainer { get => levelContainer; set => levelContainer = value; }
    public bool IsTurn { get => isTurn; set => isTurn = value; }

    public void Awake()
    {
        startRoundPanel.SetActive(true);
        round = FindObjectOfType<StartRoundPanel>();
        info = FindObjectOfType<InfoPanel>();
    }

    public void Start()
    {
        //загружаем сохранение
        //save.Load();

        //выгружаем префаб уровня
        LevelContainer = Instantiate(Resources.Load<GameObject>("Prefabs/Levels/Level " + level));
        data = FindObjectOfType<LevelData>();
        //устанавливаем его на место
        LevelContainer.transform.SetParent(boardPanel.transform, false);
        //показываем инфу уровня на информационной панели
        info.ShowRoundInfo(level);
        //и на вводной панели
        round.ShowRoundInfo(Level);
        //проигрываем анимацию вводной панели
        startRoundPanel.GetComponent<Animation>().Play("Start Round Panel");
    }

    public void ShowTraining()
    {
        //если режим обучения включен
        if (IsTraining)
        {
            GameObject tutorialLayer = Instantiate(Resources.Load<GameObject>("Prefabs/UI/Panels/Tutorial Panel"));
            tutorialLayer.transform.SetParent(boardPanel.transform, false);

            tooltip = tutorialLayer.GetComponentInChildren<TooltipPanel>();

            if (level == 1)
            {
                tooltip.ShowTooltip(1);
                tooltip.BttnsPanel.SetActive(true);
            }
            StartCoroutine(Training());
        }
    }

    IEnumerator Training()
    {
        tooltip.GetComponent<Animation>().Play("Tooltip Panel Slide");
        yield return new WaitForSeconds(0.6f);
        if (level == 1)
        {
            tooltip.transform.parent.GetChild(1).GetComponent<Animation>().Play("Finger 1");
        }
        yield return new WaitForSeconds(2.1f);
        tooltip.transform.parent.GetChild(1).gameObject.SetActive(false);
        tooltip.transform.parent.GetChild(2).gameObject.SetActive(true);
    }

}


