﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.UI;
using UnityEngine;
using UnityEngine.UI;

public class Board : MonoBehaviour
{
    [SerializeField] int width;
    [SerializeField] int height;

    GameObject[,] cells;

    public GameObject[,] BuildBoard()
    {
        width = GetComponent<GridLayoutGroup>().constraintCount;
        height = transform.childCount / width;

        cells = new GameObject[height, width];

        int temp = 0;

        for (int h = 0; h < height; h++)
        {
            for (int w = 0; w < width; w++)
            {
                cells[h, w] = transform.GetChild(temp).gameObject;
                temp++;
            }
        }
        return cells;
    }





    //    int possibleMoves;
    //    [SerializeField] GameObject cellPref;
    //    LevelController controller;
    //    List<Sprite> allSprites;

    //    public GameObject[,] BuildBoard(int level)
    //    {

    //        controller = FindObjectOfType<LevelController>();
    //        allSprites = controller.AllSprites;

    //        if (level == 1)
    //        {
    //            width = 9;
    //            height = 9;
    //        }

    //        cells = new GameObject[height, width];

    //        GetComponent<GridLayoutGroup>().constraintCount = width;
    //        for (int h = 0; h < height; h++)
    //        {
    //            for (int w = 0; w < width; w++)
    //            {
    //                GameObject cell = Instantiate(cellPref);
    //                cell.transform.SetParent(transform, false);
    //                cell.GetComponent<Cell>().HPos = h;
    //                cell.GetComponent<Cell>().WPos = w;
    //                cells[h, w] = cell;
    //            }
    //        }

    //        bool check = false;
    //        while (check == false)
    //        {
    //            FillBoard();

    //            if (CheckMoveability())
    //            {
    //                Debug.Log("Has move");
    //                check = true;
    //            }
    //        }

    //        return cells;
    //    }

    //    void FillBoard()
    //    {
    //        for (int h = 0; h < height; h++)
    //        {
    //            for (int w = 0; w < width; w++)
    //            {
    //                List<Sprite> tempSpriteList = new List<Sprite>();
    //                tempSpriteList.AddRange(allSprites);
    //                if (h > 1)
    //                {
    //                    if (cells[h - 1, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h - 2, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                    {
    //                        tempSpriteList.Remove(cells[h - 1, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite);
    //                    }
    //                }
    //                if (w > 1)
    //                {
    //                    if (cells[h, w - 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h, w - 2].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                    {
    //                        tempSpriteList.Remove(cells[h, w - 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite);
    //                    }
    //                }
    //                if (h < height - 2)
    //                {
    //                    if (cells[h + 1, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h + 2, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                    {
    //                        tempSpriteList.Remove(cells[h + 1, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite);
    //                    }
    //                }
    //                if (w < width - 2)
    //                {
    //                    if (cells[h, w + 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h, w + 2].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                    {
    //                        tempSpriteList.Remove(cells[h, w + 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite);
    //                    }
    //                }

    //                int random = Random.Range(0, tempSpriteList.Count);
    //                cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite = tempSpriteList[random];
    //            }
    //        }
    //    }

    //    bool CheckMoveability()
    //    {
    //        for (int h = 0; h < height; h++)
    //        {
    //            for (int w = 0; w < width; w++)
    //            {
    //                //если впереди есть три спрайта
    //                if (w < width - 3)
    //                {
    //                    //если текущая и следующая ячейки по горизонтали содержат одинаковые спрайты,
    //                    if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h, w + 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                    {
    //                        //проверяем третий спрайт в этой строке
    //                        if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h, w + 3].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                        {
    //                            return true;
    //                        }
    //                    }
    //                }

    //                //если впереди есть два спрайта
    //                if (w < width - 2)
    //                {
    //                    //если текущая и следующая ячейки по горизонтали содержат одинаковые спрайты
    //                    if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h, w + 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                    {
    //                        //если это не последняя строка
    //                        if (h < height - 1)
    //                        {
    //                            //проверяем второй спрайт в следующей строке
    //                            if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                            == cells[h + 1, w + 2].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                        //если это не первая строка
    //                        if (h > 0)
    //                        {
    //                            //проверяем второй спрайт в предыдущей строке
    //                            if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                            == cells[h - 1, w + 2].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                    }
    //                    //если текущая и через одну ячейки по горизонтали содержат одинаковые спрайты,
    //                    if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h, w + 2].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                    {
    //                        //если это не последняя строка
    //                        if (h < height - 1)
    //                        {
    //                            //проверяем второй спрайт в следующей строке
    //                            if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                            == cells[h + 1, w + 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                        //если это не первая строка
    //                        if (h > 0)
    //                        {
    //                            //проверяем второй спрайт в предыдущей строке
    //                            if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                            == cells[h - 1, w + 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                    }
    //                }

    //                //если позади есть два спрайта
    //                if (w > 1)
    //                {
    //                    //если текущая и предыдущая ячейки по горизонтали содержат одинаковые спрайты,
    //                    if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h, w - 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                    {
    //                        //если это не последняя строка
    //                        if (h < height - 1)
    //                        {
    //                            //проверяем второй спрайт назад в следующей строке
    //                            if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                            == cells[h + 1, w - 2].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                        //если это не первая строка
    //                        if (h > 0)
    //                        {
    //                            //проверяем второй спрайт назад в предыдущей строке
    //                            if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                            == cells[h - 1, w - 2].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                    }
    //                }

    //                //если позади есть три спрайта
    //                if (w > 2)
    //                {
    //                    //если текущая и предыдущая ячейки по горизонтали содержат одинаковые спрайты,
    //                    if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h, w - 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                    {
    //                        //проверяем третий спрайт назад в этой строке
    //                        if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h, w - 3].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                        {
    //                            return true;
    //                        }
    //                    }
    //                }

    //                //если вниз есть три спрайта
    //                if (h < height - 3)
    //                {
    //                    //если текущая и следующая ячейки по вертикали содержат одинаковые спрайты,
    //                    if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h + 1, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                    {
    //                        //проверяем третий спрайт в этой строке
    //                        if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h + 3, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                        {
    //                            return true;
    //                        }
    //                    }
    //                }

    //                //если вниз есть два спрайта
    //                if (h < height - 2)
    //                {
    //                    //если текущая и следующая ячейки по вертикали содержат одинаковые спрайты,
    //                    if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h + 1, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                    {
    //                        //если это не последний столбец
    //                        if (w < width - 1)
    //                        {
    //                            //проверяем третий спрайт в этой строке
    //                            if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                                == cells[h + 2, w + 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                        //если это не первый столбец
    //                        if (w > 0)
    //                        {
    //                            //проверяем третий спрайт в этой строке
    //                            if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                                == cells[h + 2, w - 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                    }
    //                    //если текущая и через одну ячейки по вертикали содержат одинаковые спрайты,
    //                    if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h + 2, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                    {
    //                        //если это не последний столбец
    //                        if (w < width - 1)
    //                        {
    //                            //проверяем третий спрайт в этой строке
    //                            if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                                == cells[h + 1, w + 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                        //если это не первый столбец
    //                        if (w > 0)
    //                        {
    //                            //проверяем третий спрайт в этой строке
    //                            if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                                == cells[h + 1, w - 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                    }
    //                }

    //                //если вверх есть два спрайта
    //                if (h > 1)
    //                {
    //                    //если текущая и следующая ячейки по вертикали содержат одинаковые спрайты,
    //                    if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h - 1, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                    {
    //                        //если это не последний столбец
    //                        if (w < width - 1)
    //                        {
    //                            //проверяем третий спрайт в этой строке
    //                            if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                                == cells[h - 2, w + 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                        //если это не первый столбец
    //                        if (w > 0)
    //                        {
    //                            //проверяем третий спрайт в этой строке
    //                            if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                                == cells[h - 2, w - 1].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                    }
    //                }

    //                //если вверх есть три спрайта
    //                if (h > 2)
    //                {
    //                    //если текущая и предыдущая ячейки по вертикали содержат одинаковые спрайты,
    //                    if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h - 1, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                    {
    //                        //проверяем третий спрайт в этой строке
    //                        if (cells[h, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite
    //                        == cells[h - 3, w].transform.GetChild(1).gameObject.GetComponent<Image>().sprite)
    //                        {
    //                            return true;
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        return false;
    //    }
}




