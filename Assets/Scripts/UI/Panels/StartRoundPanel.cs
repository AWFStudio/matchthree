﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartRoundPanel : MonoBehaviour
{
    GoalTextPanel text;
    GoalsPanel goals;
    LevelController controller;

    public void ShowRoundInfo(int level)
    {
        controller = FindObjectOfType<LevelController>();

        text = GetComponentInChildren<GoalTextPanel>();
        text.ShowGoalText();

        goals = GetComponentInChildren<GoalsPanel>();
        goals.ShowGoals();
    }

    public void Press()
    {
        StartCoroutine(ClosePanel());
    }

    IEnumerator ClosePanel()
    {
        GetComponent<Animation>().Play("Start Round Panel 2");
        yield return new WaitForSeconds(0.5f);
        controller.ShowTraining();
        yield return new WaitForSeconds(0.5f);
        gameObject.SetActive(false);
    }

}
