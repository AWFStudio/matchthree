﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ProgressPanel : MonoBehaviour
{
    GameObject goal_1;
    GameObject goal_2;
    GameObject goal_3;

    LevelData data;

    [SerializeField] int goal_1_progress = 0;
    [SerializeField] int goal_2_progress = 0;
    [SerializeField] int goal_3_progress = 0;

    public void ShowGoals()
    {
        goal_1 = transform.GetChild(0).gameObject;
        goal_2 = transform.GetChild(1).gameObject;
        goal_3 = transform.GetChild(2).gameObject;

        data = FindObjectOfType<LevelData>();

        if (data.Sprite_1_amount != 0)
        {
            goal_1.SetActive(true);
            goal_1_progress = goal_1.GetComponent<GoalPanel>().ShowGoalInfo(1);
        }
        if (data.Sprite_2_amount != 0)
        {
            goal_2.SetActive(true);
            goal_2_progress = goal_2.GetComponent<GoalPanel>().ShowGoalInfo(2);
        }
        if (data.Sprite_3_amount != 0)
        {
            goal_3.SetActive(true);
            goal_3_progress = goal_3.GetComponent<GoalPanel>().ShowGoalInfo(3);
        }
    }

    public void UpdateProgress(int goal)
    {
        if (goal == 0)
        {
            goal_1_progress = transform.GetChild(goal).GetComponent<GoalPanel>().UpdateGoalInfo();
        }
        else if (goal == 1)
        {
            goal_2_progress = transform.GetChild(goal).GetComponent<GoalPanel>().UpdateGoalInfo();
        }
        else if (goal == 2)
        {
            goal_3_progress = transform.GetChild(goal).GetComponent<GoalPanel>().UpdateGoalInfo();
        }

        if (goal_1_progress == 0
            && goal_2_progress == 0
            && goal_3_progress == 0)
        {
            //уровень пройден
        }

    }

}
