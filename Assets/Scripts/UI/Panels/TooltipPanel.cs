﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TooltipPanel : MonoBehaviour
{
    TextAsset asset;
    XMLSettings UIelement;

    [SerializeField] TextMeshProUGUI tooltipText;
    [SerializeField] GameObject bttnsPanel;

    public GameObject BttnsPanel { get => bttnsPanel; set => bttnsPanel = value; }

    public void ShowTooltip(int tooltipMessage)
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/Tooltips");
        UIelement = XMLSettings.Load(asset);
        tooltipText.text = UIelement.UIelements[tooltipMessage].text;
    }

}
