﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class MovesPanel : MonoBehaviour
{
    int moves;
    LevelData data;

    [SerializeField] TextMeshProUGUI movesText;

    public void ShowMovesInfo()
    {
        data = FindObjectOfType<LevelData>();
        moves = data.Moves;
        movesText.text = moves.ToString();
    }

    public void UpdateMovesInfo()
    {
        moves--;
        movesText.text = moves.ToString();
        if (moves == 0)
        {
            //мы проиграли
        }
    }
}

