﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GoalPanel : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI counterText;
    [SerializeField] Image image;
    [SerializeField] int counter;

    LevelData data;

    public int ShowGoalInfo(int goalNum)
    {
        if (!data)
        {
            data = FindObjectOfType<LevelData>();
        }

        if (goalNum == 1)
        {
            image.sprite = data.CollectionSprite_1;
            counter = data.Sprite_1_amount;
        }
        else if (goalNum == 2)
        {
            image.sprite = data.CollectionSprite_2;
            counter = data.Sprite_2_amount;
        }
        else if (goalNum == 3)
        {
            image.sprite = data.CollectionSprite_3;
            counter = data.Sprite_2_amount;
        }
        counterText.text = counter.ToString();
        return counter;

    }

    public int UpdateGoalInfo()
    {
        counter--;
        counterText.text = counter.ToString();
        return counter;
    }
}
