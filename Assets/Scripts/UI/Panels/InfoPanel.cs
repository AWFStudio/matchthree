﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InfoPanel : MonoBehaviour
{
    TextAsset asset;
    XMLSettings UIelement;

    ItemImg itemImg;
    MovesPanel moves;
    ProgressPanel progress;

    [SerializeField] TextMeshProUGUI headerText;

    private void Awake()
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI");
        UIelement = XMLSettings.Load(asset);

        itemImg = GetComponentInChildren<ItemImg>();
        moves = GetComponentInChildren<MovesPanel>();
        progress = GetComponentInChildren<ProgressPanel>();
    }

    public void ShowRoundInfo(int level) //показываем первичную инфу уровня
    {
        Awake();

        //текущий уровень
        headerText.text = UIelement.UIelements[0].text + level.ToString();
        //айтем уровня
        itemImg.SetItemImage();
        //остаток ходов
        moves.ShowMovesInfo();
        //цели уровня
        progress.ShowGoals();
    }
}
