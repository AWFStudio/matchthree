﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalsPanel : MonoBehaviour
{
    GameObject goal_1;
    GameObject goal_2;
    GameObject goal_3;

    LevelData data;

    public void ShowGoals()
    {
        goal_1 = transform.GetChild(0).gameObject;
        goal_2 = transform.GetChild(1).gameObject;
        goal_3 = transform.GetChild(2).gameObject;

        data = FindObjectOfType<LevelData>();

        if (data.Sprite_1_amount != 0)
        {
                goal_1.SetActive(true);
                goal_1.GetComponent<GoalPanel>().ShowGoalInfo(1);
        }
        if (data.Sprite_2_amount != 0)
        {
                goal_2.SetActive(true);
                goal_2.GetComponent<GoalPanel>().ShowGoalInfo(2);
        }        
        if (data.Sprite_3_amount != 0)
        {
                goal_3.SetActive(true);
                goal_3.GetComponent<GoalPanel>().ShowGoalInfo(3);
        }
    }
}