﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class GoalTextPanel : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI goalText;

    TextAsset asset;
    XMLSettings UIelement;

    LevelData data;

    public void ShowGoalText()
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/Goals");
        UIelement = XMLSettings.Load(asset);

        data = FindObjectOfType<LevelData>();

        goalText.text = UIelement.UIelements[data.GoalText].text;
    }
}
