﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemImg : MonoBehaviour
{
    [SerializeField] Image image;
    LevelData data;
    public void SetItemImage()
    {
        data = FindObjectOfType<LevelData>();
        image.sprite = data.ItemImage;
    }
}
