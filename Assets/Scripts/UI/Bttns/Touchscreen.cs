﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Touchscreen : MonoBehaviour, IPointerDownHandler
{
    GameObject tutorialPanel;

    void Awake()
    {
        tutorialPanel = FindObjectOfType<TutorialPanel>().gameObject;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Destroy(tutorialPanel);
    }

}
