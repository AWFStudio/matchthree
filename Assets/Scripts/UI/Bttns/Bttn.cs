﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class Bttn : MonoBehaviour
{
    enum assignment
    {
        skipTraining
    }

    [SerializeField] assignment buttonAssignment;
    [SerializeField] TextMeshProUGUI bttnText;

    TextAsset asset;
    XMLSettings UIelement;
    SaveManager save;

    void Start()
    {
        save = FindObjectOfType<SaveManager>();

        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/Bttns");
        UIelement = XMLSettings.Load(asset);

        if (buttonAssignment == assignment.skipTraining)
        {
            bttnText.text = UIelement.UIelements[0].text;
        }
    }

    public void Press()
    {
        if (buttonAssignment == assignment.skipTraining)
        {
            LevelController controller = FindObjectOfType<LevelController>();
            controller.IsTraining = false;
            save.Save();
            GetComponentInParent<TutorialPanel>().gameObject.SetActive(false);
        }
    }
}
