﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tile : MonoBehaviour
{
    float speed = 5f;
    GameObject oldCell;
    LevelController controller;
    ProgressPanel progress;
    Canvas canvas;
    ChainFinder finder;
    MovesPanel moves;
    ChainDestructor destructor;

    bool isCollecting = false;

    private void Awake()
    {
        controller = FindObjectOfType<LevelController>();
        canvas = FindObjectOfType<Canvas>();
        progress = FindObjectOfType<ProgressPanel>();
        finder = FindObjectOfType<ChainFinder>();
        moves = FindObjectOfType<MovesPanel>();
        destructor = FindObjectOfType<ChainDestructor>();
    }

    public void MoveTo(GameObject newCell, bool isCheck)
    {
        controller.IsTurn = true;
        StartCoroutine(Move(newCell, isCheck));
    }

    IEnumerator Move(GameObject newCell, bool isCheck)
    {

        //запоминаем ячейку, из которой мы этот тайл тащили
        oldCell = transform.parent.gameObject;
        //устанавливаем для тайла новую родительскую ячейку
        transform.SetParent(newCell.transform, true);
        //двигаем тайл, пока он не переместился на новое место, почти вплотную
        while (Vector3.Distance(transform.position, transform.parent.position) > 0.01f)
        {
            transform.position = Vector3.MoveTowards(transform.position, transform.parent.position, Time.deltaTime * speed);
            yield return null;
        }
        //потом ставим его четко на нужную позицию (вдруг немного не до конца доехал)
        transform.position = transform.parent.position;

        yield return new WaitForSeconds(0.1f);

        //если этот тайл нуждается в проверке на возникновение новых цепочек
        if (isCheck)
        {
            List<GameObject> tilesChain = new List<GameObject>();
            tilesChain.AddRange(finder.CheckForAssembledChains());

            //если после перемещения тайла не сложилось цепочек
            if (tilesChain.Count == 0)
            {
                //то перемещаем тайлы на прежние места
                oldCell.GetComponent<Cell>().GetTile().GetComponent<Tile>().MoveBack(transform.parent.gameObject);
                StartCoroutine(Back(oldCell));
            }
            else //иначе
            {
                //минусуем ход
                moves.UpdateMovesInfo();
                //определяем, смещение по какой оси привело к образованию цепочки (это нужно для правильной выдачи бонуса)
                bool isHorizontal = false;
                if (oldCell.transform.position.x != transform.position.x)
                {
                    isHorizontal = true;
                }
                //вызываем метод уничтожения цепочек
                destructor.ChainsDestruction(tilesChain, gameObject, isHorizontal);
            }
        }
    }

    public void MoveBack(GameObject cell)
    {
        StartCoroutine(Back(cell));
    }

    IEnumerator Back(GameObject cell)
    {
        //меняем родителя
        transform.SetParent(cell.transform, true);

        //двигаем, пока не встанет на место (почти)
        while (Vector3.Distance(transform.position, transform.parent.position) > 0.01f)
        {
            transform.position = Vector3.MoveTowards(transform.position, transform.parent.position, Time.deltaTime * speed);
            yield return null;
        }
        //поправляем местоположение
        transform.position = transform.parent.position;
        controller.IsTurn = false;
    }

    public void CollectThisTile(int goalNum)
    {
        if (!isCollecting)
        {
            isCollecting = true;
            transform.SetParent(canvas.transform, true);
            StartCoroutine(MoveToGoalPanel(goalNum));
        }
    }

    IEnumerator MoveToGoalPanel(int goalNum)
    {
        Vector3 position = progress.gameObject.transform.GetChild(goalNum).transform.position;
        while (Vector3.Distance(transform.position, position) > 0.01f)
        {
            transform.position = Vector3.MoveTowards(transform.position, position, Time.deltaTime * speed * 2);
            yield return null;
        }
        progress.UpdateProgress(goalNum);
        Destroy(gameObject);
    }

    public void MoveToBonusPos(GameObject cell)
    {
        transform.SetParent(canvas.transform, true);
        StartCoroutine(MoveToBonusPosAndDestroy(cell));
    }


    IEnumerator MoveToBonusPosAndDestroy(GameObject cell)
    {
        Vector3 position = cell.transform.position;
        while (Vector3.Distance(transform.position, position) > 0.01f)
        {
            transform.position = Vector3.MoveTowards(transform.position, position, Time.deltaTime * speed * 2);
            yield return null;
        }
        Destroy(gameObject);
    }
}





