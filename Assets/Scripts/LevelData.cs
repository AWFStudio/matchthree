﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelData : MonoBehaviour
{
    [SerializeField] int moves;
    [SerializeField] int goalText;
    [SerializeField] Sprite itemImage;

    [SerializeField] Sprite collectionSprite_1;
    [SerializeField] int sprite_1_amount;

    [SerializeField] Sprite collectionSprite_2;
    [SerializeField] int sprite_2_amount;

    [SerializeField] Sprite collectionSprite_3;
    [SerializeField] int sprite_3_amount;

    [SerializeField] List<Sprite> allSprites; //все возможные спрайты тайлов

    public int Moves { get => moves; set => moves = value; }
    public Sprite ItemImage { get => itemImage; set => itemImage = value; }
    public int Sprite_1_amount { get => sprite_1_amount; set => sprite_1_amount = value; }
    public int Sprite_2_amount { get => sprite_2_amount; set => sprite_2_amount = value; }
    public int Sprite_3_amount { get => sprite_3_amount; set => sprite_3_amount = value; }
    public Sprite CollectionSprite_1 { get => collectionSprite_1; set => collectionSprite_1 = value; }
    public Sprite CollectionSprite_2 { get => collectionSprite_2; set => collectionSprite_2 = value; }
    public Sprite CollectionSprite_3 { get => collectionSprite_3; set => collectionSprite_3 = value; }
    public int GoalText { get => goalText; set => goalText = value; }
    public List<Sprite> AllSprites { get => allSprites; set => allSprites = value; }
}
