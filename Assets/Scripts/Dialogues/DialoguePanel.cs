﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class DialoguePanel : MonoBehaviour
{
    //текстовые поля
    [SerializeField] TextMeshProUGUI messageText;
    [SerializeField] TextMeshProUGUI leftCharacterName;
    [SerializeField] TextMeshProUGUI rightCharacterName;

    //панель подсказки
    [SerializeField] GameObject tooltipPanel;

    //скрипт побуквенного вывода текста
    TextWriter.TextWriterSingle textWriterSingle;

    //первая строка диалога
    int firstLine;
    //последняя строка диалога
    int lastLine;
    //шаг
    int step;
    //номер диалога
    int dialogueNum;
    //ID персонажей
    int leftCharacterID;
    int rightCharacterID;

    //массив строк текста текущей сессии
    string[] messageArray;

    //ассет текста
    TextAsset asset;
    XMLSettings UIelement;

    GameObject tooltip;
    Canvas canvas;
    GameController game;
    SaveManager save;

    private void Awake()
    {
        canvas = FindObjectOfType<Canvas>();
        game = FindObjectOfType<GameController>();
        save = FindObjectOfType<SaveManager>();
    }

    public void ShowDialog(int dialogueNum)
    {
        this.dialogueNum = dialogueNum;

        //подгружаем ассет
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/Dialog");
        UIelement = XMLSettings.Load(asset);

        //выбираем строки текста для показа в этой сессии
        AddLines(dialogueNum);
        //устанавливаем шаг
        step = 0;
        //запускаем вывод первой строки
        textWriterSingle = TextWriter.AddWriter_Static(messageText, messageArray[step], 0.05f, true, true);

        ShowCharacterName();
    }

    public void ShowCharacterName()
    {
        //подгружаем ассет
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/Characters");
        UIelement = XMLSettings.Load(asset);

        if (dialogueNum == 1)
        {
            leftCharacterID = 0;

            leftCharacterName.text = UIelement.UIelements[leftCharacterID].text;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown("space")
            || Input.GetKeyDown(KeyCode.Return))
        {
            ForwardBttnPress();
        }
    }

    //кнопка "вперед"
    public void ForwardBttnPress()
    {
        if (tooltipPanel.activeSelf)
        {
            tooltipPanel.SetActive(false);
        }

        //если вывод строки еще не  закончен
        if (textWriterSingle != null && textWriterSingle.IsActive())
        {
            //показываем строку целиком
            textWriterSingle.WriteAllAndDestroy();
        }
        else //иначе
        {
            //делаем шаг по массиву строк
            step++;
            if (step < messageArray.Length) //если есть строка для вывода
            {
                //выводим ее
                textWriterSingle = TextWriter.AddWriter_Static(messageText, messageArray[step], 0.05f, true, true);
            }
            else //иначе сохраняем инфу о том, что диалог просмотрен и запускаем уровень
            {
                game.IsDialogue = true;
                save.Save();
                SceneManager.LoadScene(1);
            }
        }

        ShowCharacterName();
    }

    void AddLines(int dialogueNum)
    {
        //выбираем строки сессии, в зависимости от номера диалога
        if (dialogueNum == 1)
        {
            firstLine = 0;
            lastLine = 2;
            StartCoroutine(ShowFirstDialogTooltip());
        }

        //инициализируем массив
        messageArray = new string[lastLine - firstLine + 1];
        //устанавливаем начальную строку
        int currentLine = firstLine;
        //заполняем массив
        for (int i = firstLine; i < lastLine + 1; i++)
        {
            messageArray[i] = UIelement.UIelements[currentLine].text;
            currentLine++;
        }
    }

    IEnumerator ShowFirstDialogTooltip()
    {
        yield return new WaitForSeconds(1f);
        tooltipPanel.SetActive(true);
        tooltipPanel.GetComponent<TooltipPanel>().ShowTooltip(0);
    }
}
