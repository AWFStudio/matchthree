﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

//добавить звук?

public class TextWriter : MonoBehaviour
{
    static TextWriter instance;
     
    private List<TextWriterSingle> textWriterSingleList;


    private void Awake()
    {
        instance = this;
        textWriterSingleList = new List<TextWriterSingle>();
    }

    public static TextWriterSingle AddWriter_Static(TextMeshProUGUI uiText, string textToWrite, float timePerCharacter, bool invisibleCharacters, bool removeWriterBeforeAdd)
    {
        if(removeWriterBeforeAdd)//если одновременно с вызовом метода поступила команда удалить текущую строку
        {
            //то сначала удаляем ее... (вызываем метод)
            instance.RemoveWriter(uiText);
        }
        //...и только потом загружаем новую
        return instance.AddWriter(uiText, textToWrite, timePerCharacter, invisibleCharacters);
    }
    private TextWriterSingle AddWriter(TextMeshProUGUI uiText, string textToWrite, float timePerCharacter, bool invisibleCharacters)
    {
        //в список экземпляров добавляем новый
        TextWriterSingle textWriterSingle = new TextWriterSingle(uiText, textToWrite, timePerCharacter, invisibleCharacters);
        textWriterSingleList.Add(textWriterSingle);
        return textWriterSingle;
    }

    public static void RemoveWriter_Static(TextMeshProUGUI uiText)
    {
        instance.RemoveWriter(uiText);
    }

    void RemoveWriter(TextMeshProUGUI uiText)//метод, чтобы удалять старую строку и загружать новую, когда нажимаем кнопку "продолжить"
    {
        for (int i = 0; i < textWriterSingleList.Count; i++) //пробегаемся по всему массиву экземпляров
        {
            if(textWriterSingleList[i].GetUIText() == uiText) //если находим тот, который нужно удалить
            {
                textWriterSingleList.RemoveAt(i); //удаляем его из массива
                i--; //не забываем укоротить массив
            }
        }
    }

    private void Update()
    {
        for (int i = 0; i < textWriterSingleList.Count; i++) //пробегаемся по всему массиву экземпляров
        {
           bool destroyInstance = textWriterSingleList[i].Update(); //проверяем, завершился ли вывод текста в этом экземпляре
            if(destroyInstance) //если да, то он нам больше не нужен
            {
                textWriterSingleList.RemoveAt(i); //удаляем его
                i--; //укорачиваем массив
            }
        }
    }

    //создаем экземпляр скрипта (на случай, если нам понадобится выводить несколько текстовых массивов одновременно)
    public class TextWriterSingle
    {
        TextMeshProUGUI uiText; //куда выгружаем текст
        string textToWrite; //текст, который выгружаем
        int characterIndex;
        float timePerCharacter; //через какое время показываем следующий символ
        float timer;
        bool invisibleCharacters; //остались ли еще непоказанные символы в строке

        //конструктор
        public TextWriterSingle(TextMeshProUGUI uiText, string textToWrite, float timePerCharacter, bool invisibleCharacters)
        {
            this.uiText = uiText;
            this.textToWrite = textToWrite;
            this.timePerCharacter = timePerCharacter;
            this.invisibleCharacters = invisibleCharacters;
            characterIndex = 0;
        }

        public bool Update()
        {
                //"тикаем" таймером
                timer -= Time.deltaTime;
                //если наступило время срабатывания таймера
                while (timer <= 0f)
                {
                    //устанавливаем время следующего срабатывания
                    timer += timePerCharacter;
                    //и определяем длину части строки, которая должна стать видимой на этом тике
                    characterIndex++;
                    //берем этот кусок строки
                    string text = textToWrite.Substring(0, characterIndex);
                    //если в строке еще есть невидимые (символы, которые еще рано показывать на этом тике) 
                    if (invisibleCharacters)
                    {
                        //прибавляем их к видимой части строки, но оставляем их невидимыми
                        text += "<color=#00000000>" + textToWrite.Substring(characterIndex) + "</color>";
                    }
                    //выводим текст (как видимую, так и невидимую части)
                    uiText.text = text;
                    //если все символы строки уже стали видимыми
                    if (characterIndex >= textToWrite.Length)
                    {
                        //обнуляем текст, предназначенный для вывода
                        uiText = null;
                    //выходим из метода
                    return true;
                    }
                }
                return false;
        }

        public TextMeshProUGUI GetUIText()
        {
            return uiText;
        }

        public bool IsActive()//проверка, показан ли уже весь текст, или еще нет (для корректной работы кнопки "продолжить")
        {
            return characterIndex < textToWrite.Length;
        }

        public void WriteAllAndDestroy() //выводим текст сразу и удаляем экземпляр
        {
            uiText.text = textToWrite;
            characterIndex = textToWrite.Length;
            TextWriter.RemoveWriter_Static(uiText);
        }
    }
}
